const Joi = require('joi')

const schemas = {
    blogPOST: Joi.object().keys({
        title: Joi.string().required(),
        description: Joi.string().required(),
    })
}

module.exports = schemas;
const Joi = require("joi"); 
app.post("/register", async (req, res) => {

  try {

    // Define Schema - object of everything you're validating and validation constraints

    const schema = Joi.object({
      username: Joi.string().min(6).alphanum().uppercase().required(),
      password:Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),                                            
      confirm_password:Joi.string().equal(Joi.ref('password')).messages({'any.only': 'password does not match' }).required(),
      firstname: Joi.string().required(),
      lastname: Joi.string(),
      email: Joi.string().email({minDomainSegments: 2}).required(),
      phonenumber: Joi.string().min(6).regex(/^([+])?(\d+)$/).required(),
      dob: Joi.date().max('01-01-2003').iso().messages({'date.format': `Date format is YYYY-MM-DD`,'date.max':`Age must be 18+`}).required(),
      sex: Joi.string().valid('male', 'female','transger', 'others')

    });

    // Validate req.body against the defined schema
    const validation = schema.validate(req.body);
    const { value, error } = validation;

    // Takes in data from body request and validates it against schema already defined
    if (error) {
      const message = error.details.map(x => x.message);

      res.status(400).json({
        status: "error",
        message: "Invalid request data",
        data: message
      });

    
    } else {
      res.json({
        status: "success",
        message: "Registration successful",
        data: value
      });
    }
  } catch (error) {
    res.json({status:"failed",message:error.message})
  }
});
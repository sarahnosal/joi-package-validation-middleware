const Joi = require('joi')

function validateUser(user) {
    const JoiSchema = Joi.object({
        username: Joi.string().alphanum().min(3).max(30).required(),
        password: Joi.string().pattern(/^[a-zA-Z0-9]{3,30}$/).min(5).required(),
        birth_year: Joi.number().integer().min(1900).max(2013).required(),
        email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } } )
    }).options({ abortEarly: false })

    return JoiSchema.validate(user)
}

const user = { 
    username: 'duchess', 
    password: 'duchess1130',
    birth_year: '1992',
    email: 'nosalsm@gmail.com'
}

const response = validateUser(user)

if(response.error) {
    console.log(response.error.message)
} else {
    console.log('Data is valid')
    console.log(response.value)
}